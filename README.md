# Search Engine:
by Clément de Araujo
    
    
### Setting up

```bash
mkdir build
cd build
cmake
```

### usage:

```bash
./searchengine [INDEX FILE] [SOURCE DIR]
```

### behavior:

if `[INDEX FILE]` does not exist, generates it with the files
contained in `[SOURCE DIR]`.
if `[INDEX FILE]` exists, reads it and open an interactive mode
to search, search AND, search OR.