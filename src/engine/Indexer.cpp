/**
 * Created by Dwerynith on 2019-06-07.
 */

#include <iostream>

#include "Document.hh"
#include "Index.hh"
#include "Indexer.hh"
#include "Normalizer.hh"
#include "TokenizedDocument.hh"

namespace engine {
    using namespace tools;

    Indexer::Indexer(const std::string& path, const std::vector<Normalizer>& processors) {
        std::cout << "    - Reading files." << std::endl;

        tokenDocs_ = std::vector<TokenizedDocument>();

        auto docs = Document::fetch(path);
        tokenDocs_.reserve(docs.size());

        std::cout << "    - found " + std::to_string(docs.size()) + " files." << std::endl;
        std::cout << "    - Tokenizing and applying processors." << std::endl;

        int i = 0;
        for (auto& doc : docs) {
            tools::printBar((float) i / (float) docs.size(), 100);
            tokenDocs_.emplace_back(TokenizedDocument(doc, processors));
            ++i;
        }
        tools::printBar(1., 100);
        std::cout << std::endl;
    }

    void Indexer::generate() {
        std::cout << "    - Generating Posting list." << std::endl;
        std::vector<Posting> postings = Posting::index(tokenDocs_);
        std::cout << "    - got " + std::to_string(postings.size()) + " Postings." << std::endl;
        std::cout << "    - Generating Index." << std::endl;
        index_ = Index(postings);
    }

    void Indexer::save(const std::string& path) {
        index_.save(path);
    }
}