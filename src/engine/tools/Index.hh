/**
 * Created by Dwerynith on 2019-06-07.
 */

#pragma once

#include <string>
#include <map>
#include <vector>
#include "Posting.hh"

namespace engine::tools {
    /**
     * Prints a progress bar on the terminal with "progress * 100 % completion"
     * the bar is barWidth characters long
     * @param progress
     * @param barWidth
     */
    void printBar(float progress, int barWidth);

    /**
     * Contains the index of all documents.
     */
    class Index {
    public:
        /**
         * Contructs an Index from a vector of postings.
         * @param postings
         */
        explicit Index(const std::vector<Posting>& postings);

        /**
         * Contructs an Index from a file's path.
         * @param path
         */
        explicit Index(const std::string& path);

        /**
         * Writes the current index to the file at the given path.
         * @param path
         */
        void save(const std::string& path);

        /**
         * searches word in index.
         * @param word
         * @return a list of documents containing the word
         */
        std::vector<std::string> search(const std::string& word);

        /**
         * searches words in index.
         * @param words
         * @return a list of documents containing all the words in words.
         */
        std::vector<std::string> searchAllOf(const std::vector<std::string>& words);

        /**
         * searches words in index.
         * @param words
         * @return a list of documents containing at least one of the words in words.
         */
        std::vector<std::string> searchOneOf(const std::vector<std::string>& words);

    private:
        std::map<std::string, int> urlToDid_;
        std::map<std::string, std::vector<int>> wordToDids_;

        std::vector<std::string> idToUrl(const std::vector<int>& v);
    };
}