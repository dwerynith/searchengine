/**
 * Created by Dwerynith on 2019-06-07.
 */

#pragma once

#include <string>
#include <vector>
#include "Document.hh"
#include "TextProcessor.hh"
#include "Normalizer.hh"

namespace engine::tools {

    class TokenizedDocument {
    public:
        /**
         * Tokenized version of a document with processors on words.
         * @param document
         * @param processors
         */
        TokenizedDocument(const Document& document, const std::vector<Normalizer>& processors);

        /**
         * @return words_
         */
        const std::vector<std::string>& getWords() const;

        /**
         * @return url_
         */
        const std::string& getUrl() const;

        /**
         * @return A readable version of the document.
         */
        std::string string();

    private:
        std::vector<std::string> words_;
        std::string url_;
    };
}
