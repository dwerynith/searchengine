/**
 * Created by Dwerynith on 2019-06-07.
 */

#pragma once

#include <vector>
#include <string>

namespace engine::tools {

    /**
     * Contains a document path and content
     */
    class Document {
    public:
        Document(const std::string& url, const std::string& text);

        /**
         * Reads a folder and gets a list of all documents it contains
         * @param path
         * @param recursice
         * @return std::vector<Document>
         */
        static std::vector<Document> fetch(std::string path, bool recursice = true);

        /**
         * Print for debugging purposes
         * @return readable version of a document
         */
        std::string string();

        /**
         * @return text_
         */
        const std::string& getText() const;

        /**
         * @return url_
         */
        const std::string& getUrl() const;

    private:
        std::string text_;
        std::string url_;
    };
}