/**
 * Created by Dwerynith on 2019-06-07.
 */

#pragma once

#include "TextProcessor.hh"

namespace engine::tools {

    class Normalizer : public TextProcessor {
    public:
        /**
         * Process the word
         * @param word
         * @return std::to_lower(word)
         */
        std::string process(std::string word) override;
    };

}
