/**
 * Created by Dwerynith on 2019-06-07.
 */

#pragma once

#include <string>

namespace engine::tools {

    class TextProcessor {
    public:
        /**
         * Process the word
         * @param word
         * @return nothing it's abstract
         */
        virtual std::string process(std::string word) = 0;
    };
}
