/**
 * Created by Dwerynith on 2019-06-07.
 */

#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>

#include "Document.hh"

namespace fs = boost::filesystem;

namespace engine::tools {

    Document::Document(const std::string& url, const std::string& text)
            : text_{text}, url_{url} {}

    std::vector<Document> Document::fetch(std::string path, bool recursice) {
        std::vector<Document> res;
        for (const auto& entry : fs::directory_iterator(path)) {
            if (fs::is_directory(entry.status()) && recursice) {
                auto rec = fetch(entry.path().string());
                res.insert(res.end(), rec.begin(), rec.end());
            } else {
                auto content = std::fstream(entry.path().string());
                Document doc(entry.path().string(), std::string((std::istreambuf_iterator<char>(content)),
                                                                std::istreambuf_iterator<char>()));
                res.push_back(doc);
            }
        }
        return res;
    }

    std::string Document::string() {
        return "{ url: " + url_ + ", text: " + text_ + " }";
    }

    const std::string& Document::getText() const {
        return text_;
    }

    const std::string& Document::getUrl() const {
        return url_;
    }
}