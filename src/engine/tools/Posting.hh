/**
 * Created by Dwerynith on 2019-06-07.
 */


#pragma once

#include <string>
#include <vector>
#include "TokenizedDocument.hh"

namespace engine::tools {

    class Posting {
    public:
        /**
         * Generates a posting from parameters.
         * @param word
         * @param url
         */
        Posting(const std::string& word, const std::string& url);

        /**
         * Generate a list of words in the documents.
         * @param documents
         * @return a list of posting corresponding to the documents.
         */
        static std::vector<Posting> index(const std::vector<TokenizedDocument>& documents);

        /**
         * @return word_
         */
        const std::string& getWord() const;

        /**
         * @return url_
         */
        const std::string& getUrl() const;

    private:
        std::string word_;
        std::string url_;
    };
}

