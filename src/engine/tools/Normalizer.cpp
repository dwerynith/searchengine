/**
 * Created by Dwerynith on 2019-06-07.
 */

#include "Normalizer.hh"

#include <algorithm>

namespace engine::tools {

    std::string Normalizer::process(std::string word) {
        std::transform(word.begin(), word.end(), word.begin(), ::tolower);
        return word;
    }
}