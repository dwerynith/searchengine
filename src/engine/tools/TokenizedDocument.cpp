/**
 * Created by Dwerynith on 2019-06-07.
 */

#include <boost/tokenizer.hpp>

#include "TokenizedDocument.hh"

namespace engine::tools {

    TokenizedDocument::TokenizedDocument(const Document& document, const std::vector<Normalizer>& processors) {
        url_ = document.getUrl();
        words_ = std::vector<std::string>();
        boost::tokenizer<> tok(document.getText());
        for (boost::tokenizer<>::iterator beg = tok.begin(); beg != tok.end(); ++beg) {
            std::string word = *beg;
            for (Normalizer procesor : processors)
                word = procesor.process(word);

            words_.push_back(word);
        }
    }

    std::string TokenizedDocument::string() {
        std::string res = "{ url: " + url_ + ", text: ";
        for (const auto& word : words_) {
            res += word;
            if (word != *words_.end())
                res += ", ";
        }
        return res + " }";
    }

    const std::vector<std::string>& TokenizedDocument::getWords() const {
        return words_;
    }

    const std::string& TokenizedDocument::getUrl() const {
        return url_;
    }
}