/**
 * Created by Dwerynith on 2019-06-07.
 */

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>

#include "Index.hh"

#define END_OF_URLS "_END_OF_ULRS_SEPARATOR_"
#define END_OF_WORD "_END_OF_WORD_SEPARATOR_"

namespace engine::tools {

    void printBar(float progress, int barWidth) {
        std::cout << "[";
        int pos = barWidth * progress;
        for (int i = 0; i < barWidth; ++i) {
            if (i < pos)
                std::cout << "=";
            else if (i == pos)
                std::cout << ">";
            else
                std::cout << " ";
        }
        std::cout << "] " << int(progress * 100.0) << " %\r";
        std::cout.flush();
    }

    std::vector<std::string> Index::idToUrl(const std::vector<int>& v) {
        std::vector<std::string> res;
        for (const auto& did : v) {
            for (auto& i : urlToDid_) {
                if (i.second == did) {
                    res.push_back(i.first);
                    break;
                }
            }
        }
        return res;
    }

    Index::Index(const std::vector<Posting>& postings) {
        urlToDid_ = std::map<std::string, int>();
        wordToDids_ = std::map<std::string, std::vector<int>>();
        int i = 0;
        for (const auto& posting : postings) {
            printBar((float) i / (float) postings.size(), 100);

            if (urlToDid_.find(posting.getUrl()) != urlToDid_.end())
                urlToDid_[posting.getUrl()] = urlToDid_.size() - 1;
            int did = urlToDid_[posting.getUrl()];
            if (wordToDids_.find(posting.getWord()) == wordToDids_.end()) {
                wordToDids_.insert(std::pair(posting.getWord(), std::vector<int>({did})));
            } else {
                auto& v = wordToDids_[posting.getWord()];
                if (std::find(v.begin(), v.end(), did) == v.end()) {
                    v.push_back(did);
                }
            }
            ++i;
        }
        tools::printBar(1., 100);
        std::cout << std::endl;
    }

    Index::Index(const std::string& path) {
        urlToDid_ = std::map<std::string, int>();
        wordToDids_ = std::map<std::string, std::vector<int>>();
        std::string tok;
        std::cout << "    - Opening file " + path + "." << std::endl;
        std::ifstream f(path);
        std::cout << "    - Reading url ids in file " + path + "." << std::endl;
        f >> tok;
        while (tok != END_OF_URLS) {
            std::string url = tok;
            f >> tok;
            int id = std::stoi(tok);
            urlToDid_[url] = id;
            f >> tok;
        }
        std::cout << "    - Reading words in file " + path + "." << std::endl;
        f >> tok;
        while (f.good()) {
            std::string word = tok;
            f >> tok;
            std::vector<int> v;
            while (tok != END_OF_WORD) {
                int id = std::stoi(tok);
                v.push_back(id);
                f >> tok;
            }
            wordToDids_[word] = v;
            f >> tok;
        }
        f.close();
    }

    void Index::save(const std::string& path) {
        std::cout << "    - Opening file " + path + "." << std::endl;
        std::ofstream f(path);
        std::cout << "    - Writing url ids in file " + path + "." << std::endl;
        int i = 0;
        for (const auto& url : urlToDid_) {
            printBar((float) i / (float) urlToDid_.size(), 100);
            f << " " << url.first << " " << url.second;
            ++i;
        }
        f << '\n' << END_OF_URLS << '\n';
        tools::printBar(1., 100);
        std::cout << std::endl
                  << "    - Writing words in file " + path + "." << std::endl;
        i = 0;
        for (const auto& word : wordToDids_) {
            printBar((float) i / (float) wordToDids_.size(), 100);
            f << word.first;
            for (const auto& id : word.second) {
                f << " " << id;
            }
            ++i;
            f << '\n' << END_OF_WORD << '\n';
        }
        tools::printBar(1., 100);
        std::cout << std::endl;
        f.close();
    }

    std::vector<std::string> Index::search(const std::string& word) {
        std::vector<int> res;
        if (wordToDids_.find(word) != wordToDids_.end())
            for (const auto& did : wordToDids_[word])
                res.push_back(did);

        return idToUrl(res);
    }

    std::vector<std::string> Index::searchAllOf(const std::vector<std::string>& words) {
        std::vector<int> res;
        if (wordToDids_.find(words[0]) != wordToDids_.end())
            for (const auto& did : wordToDids_[words[0]])
                res.push_back(did);

        for (const auto& word: words) {
            for (const auto& did: res) {
                auto v = wordToDids_[word];
                auto tmp = std::find(wordToDids_[word].begin(), wordToDids_[word].end(), did);
                if (tmp == wordToDids_[word].end())
                    res.erase(std::remove(res.begin(), res.end(), did), res.end());
            }
        }

        return idToUrl(res);
    }

    std::vector<std::string> Index::searchOneOf(const std::vector<std::string>& words) {
        std::vector<int> res;
        for (const auto& word: words)
            if (wordToDids_.find(word) != wordToDids_.end())
                for (const auto& did : wordToDids_[word])
                    if (std::find(res.begin(), res.end(), did) == res.end())
                        res.push_back(did);

        return idToUrl(res);
    }
}