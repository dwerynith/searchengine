/**
 * Created by Dwerynith on 2019-06-07.
 */

#include "Document.hh"
#include "Posting.hh"

namespace engine::tools {

    Posting::Posting(const std::string& word, const std::string& url)
            : word_{word}, url_{url} {}

    std::vector<Posting> Posting::index(const std::vector<TokenizedDocument>& documents) {
        std::vector<Posting> res;
        for (const auto& doc: documents)
            for (const auto& token : doc.getWords())
                res.emplace_back(Posting(token, doc.getUrl()));

        return res;
    }

    const std::string& Posting::getWord() const {
        return word_;
    }

    const std::string& Posting::getUrl() const {
        return url_;
    }

}