/**
 * Created by Dwerynith on 2019-06-07.
 */

#include "Searcher.hh"

namespace engine {
    Searcher::Searcher(const std::string& path)
            : index_{path} {}

    std::vector<std::string> Searcher::search(const std::string& word) {
        return index_.search(word);
    }

    std::vector<std::string> Searcher::searchAllOf(const std::vector<std::string>& words) {
        return index_.searchAllOf(words);
    }

    std::vector<std::string> Searcher::searchOneOf(const std::vector<std::string>& words) {
        return index_.searchOneOf(words);
    }
}