/**
 * Created by Dwerynith on 2019-06-07.
 */

#pragma once

#include <engine/tools/Index.hh>

namespace engine {

    class Searcher {
    public:
        /**
         * Generates a searher from the indedx at the given path.
         * @param path
         */
        explicit Searcher(const std::string& path);

        /**
         * searches word in index.
         * @param word
         * @return a list of documents containing the word
         */
        std::vector<std::string> search(const std::string& word);

        /**
         * searches words in index.
         * @param words
         * @return a list of documents containing all the words in words.
         */
        std::vector<std::string> searchAllOf(const std::vector<std::string>& words);

        /**
         * searches words in index.
         * @param words
         * @return a list of documents containing at least one of the words in words.
         */
        std::vector<std::string> searchOneOf(const std::vector<std::string>& words);

    private:
        tools::Index index_;
    };
}