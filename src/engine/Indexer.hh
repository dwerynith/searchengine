/**
 * Created by Dwerynith on 2019-06-07.
 */

#pragma once

#include <string>
#include <vector>
#include <Normalizer.hh>

#include "Posting.hh"
#include "TokenizedDocument.hh"
#include "Index.hh"

namespace engine {

    class Indexer {
    public:
        /**
         * Indexer from a directory path and processors.
         * @param path
         * @param processors
         */
        explicit Indexer(const std::string& path, const std::vector<tools::Normalizer>& processors);

        /**
         * Generate the index by reading the files.
         */
        void generate();

        /**
         * saves the index to the file at the given path.
         * @param path
         */
        void save(const std::string& path);

    private:
        std::vector<tools::TokenizedDocument> tokenDocs_;
        tools::Index index_ = tools::Index(std::vector<tools::Posting>());
    };
}