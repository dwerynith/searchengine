/**
 * Created by Dwerynith on 2019-06-07.
 */

#include <fstream>
#include <iostream>

#include "Indexer.hh"
#include "Normalizer.hh"
#include "Searcher.hh"

using namespace engine;
using Normalize = tools::Normalizer;

int main(int argc, char** argv) {
    if (argc != 3) {
        std::cerr << "usage: ./searchengine [INDEX FILE] [SOURCE DIR]\n";
        return 1;
    }
    std::cout << "Search Engine by de_ara_c\n\n";

    /**
     * Generating the Index file
     */
    std::ifstream f(argv[1]);

    if (!f.good()) {
        std::cout << "Reading source dir:" << std::endl;
        Indexer indexer(argv[2], std::vector<Normalize>({Normalize()}));
        std::cout << "    - DONE." << std::endl;
        std::cout << "Generating Index:" << std::endl;
        indexer.generate();
        std::cout << "    - DONE." << std::endl;
        std::cout << "Saving Index file:" << std::endl;
        indexer.save(argv[1]);
        std::cout << "    - DONE." << std::endl;
    } else {
        std::cout << "Reading Index file:" << std::endl;
        Searcher searcher(argv[1]);
        std::cout << "    - DONE." << std::endl;
        std::cout << "Starting interactive mode:" << std::endl;
        std::string word;
        std::cout << "> ";
        std::cin >> word;
        while (std::cin.good()) {
            std::cout << "found: \n";
            for (const auto& url: searcher.search(word)) {
                std::cout << "    - " << url << "\n";
            }
            std::cout.flush();
            std::cout << "> ";
            std::cin >> word;
        }
        std::cout << "\nSearching 'aachen' AND 'adlib':\n";
        for (const auto& url: searcher.searchAllOf({"aachen", "adlib"})) {
            std::cout << "    - " << url << "\n";
        }
        std::cout << "Searching 'aachen' OR 'adlib':\n";
        for (const auto& url: searcher.searchOneOf({"aachen", "adlib"})) {
            std::cout << "    - " << url << "\n";
        }
    }
    return 0;
}